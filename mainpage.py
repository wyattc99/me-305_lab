'''@file                mainpage.py
   @brief               Brief doc for mainpage.py
   @details             Detailed doc for mainpage.py 
   @author              Wyatt Conner, James Lavelle

   @date                October 21,2021

   @mainpage

   @section sec_intro   Introduction
                        This is our project for the ME-305 Introduction to Mechatronics
                        class. This is a portfolio complied of all the labs we have
                        completed towards creating an automated mechanical system
                        to balance a ball. The link to our source code is This link [
                        <a href="https://bitbucket.org/wyattc99/me-305_lab/src/master/"> Source_Code: </a>
                        ]. 

   @section Lab_01      LED Flash Game
                        [
                        <a href="https://wyattc99.bitbucket.io/namespaceLab__0x01.html"> LAB1: </a>
                        ]
                        This is an LED Flash game in which the user will press the LED button to get various
                        LED patterns. 
            
   @section lab_02      Encoder Configuration 
                          [
                        <a href="https://wyattc99.bitbucket.io/namespaceLab__0x02.html"> LAB2: </a>
                        ]
                        This lab is a encoder configuration in which will allow a user to 
                        interact with the encoders through PUTTY. The user can obtain position
                        and delta data and collect data for 30 seconds. 
                        
  @section lab_03       Motor Configuration
                        [
                        <a href="https://wyattc99.bitbucket.io/namespaceLab__0x03.html"> LAB3: </a>
                        ]
                        This lab is a motor configuration which is built off from lab2.The user will be
                        able to interact with the motors and encoders through the putty window. This will
                        allow the user to collect speed, postion data and set the duty cycle of the motors. 
                        
  @section lab_04       Motor Speed Controller
                        [
                        <a href="https://wyattc99.bitbucket.io/namespaceLab__0x04.html"> LAB4: </a>
                        ]
                        This a speed controller of a motor equipped with an ecnoder. This lab is heavily built 
                        from lab 3. The speed controller will allow users to set a gain, and desired speed of the
                        motor through a putty window. From there the motor regulates it's speed through the data
                        given from the encoder. 
                        
  @section lab_05       I2C Driver
                        This lab is a driver for the I2C. the I2C is a device that can measure accleration,
                        displacement, and magnetic fields. This driver allows us to obtain the data by
                        writing instructions to the I2C and reading information from the I2C and then 
                        converting that data to digital information. 
                        
  @section Homework 0x02 & 0x03 
                       This homework assignment I work through deriving the state equations of motion for the
                       term project which is a platform balancing a ball. Once they equation are derived
                       then linerize them to get our final matrices for our state space which is then used
                       to simulate the model for an open and closed loop model. The report can be seen here
                       [
                        <a href="https://wyattc99.bitbucket.io/HW2and3/"> Report: </a>
                        ]
  @section Term Project
                       This is the term project for the ME-305 class. The project was to balance a ball on
                       a plat form using two motors, internal measurement unit and resistive touch pad. These
                       pieces of hardware were capable of recording the angles of the platform and location of 
                       the ball on the platform. From that we created a speed controller to zero the platform to
                       realtivly zero angle and attempt to balance the ball. Our progress is detailed in our
                       report which is linked here:
                        [
                        <a href="https://wyattc99.bitbucket.io/HW2and3/"> Term_Project: </a>
                        ]
 
'''