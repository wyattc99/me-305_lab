# -*- coding: utf-8 -*-
"""
@package            Term_Project

@file               Task_Speed_Controller.py
@brief              This file will construct a closed loop controller for the 
                    ball balancing system
@details            This class will generate a task controller object by using 
                    information and relevant values calculated from other 
                    files. The speed controllor will operate in a closed loop. 
                    The controller will need the gain found from equations and 
                    data calibrated from the other task file to run
                    efficiently.
@author             James Lavelle
@author             Wyatt Conner
@date               8 December 2021
"""

# general imports from python and shares file
import pyb
import time
import utime
from shares import Share

class Task_Controller():
    """
    @brief      This class will generate a task controller object.         
    @details    This class will generate a task controller object by using 
                information and relevant values calculated from other files.
                The speed controllor will operate in a closed loop. The 
                controller will need the gain found from equations and data 
                calibrated from the other task file to run efficiently.
    """
    
    def __init__(self, Flag, system, xcord, ycord, zcord, pitch, roll, roll_vel, pitch_vel, motor, motor_drive):
        """
        @brief              Constructs the parameters for the closed loop class
        @details            This function will construct and initialize the 
                            parameters for the closed loop class. These 
                            parameters include the gain and the pmw level.
        @param   Flag       The duty of the system.
        @param   system     The system.
        @param   xcord      The xcord from the driver.
        @param   ycord      The y cord from the driver.
        @param   pitch      The pitch.
        @param   roll       The roll.
        @param   roll_vel   The roll velocity from the IMU.
        @param   pitch_vel  The pitch velocity from the IMU.
        @param   motor      One of the two motor objects.
        @param   motor_drive The driver object of the motor.
        """

        ##the flag
        self.Flag = Flag
        ##the system 
        self.system = system
        ##the x cord from the driver
        self.xcord = xcord
        ##the y cord from the driver
        self.ycord = ycord
        ## z cord from the driver
        self.zcord = zcord
        ##the pitch from the IMU
        self.pitch = pitch
        ##the roll from the IMU
        self.roll = roll
        ##the roll velocity
        self.roll_vel = roll_vel
        ##the pitch velocity
        self.pitch_vel = pitch_vel
        ##a motor object for specific motor
        self.motor = motor
        ## motor driver object for both motors
        self.motor_drive = motor_drive
        ##the previous time of the system
        self.previousTime = time.ticks_us()
        
        # defining internal variables
        #
        ## The kp values for our motor controller
        # 1 - self.kp_weight = [-.45, -.19, -.03, -.012]
        #self.kp_weight = [ -73, -35, -5, -2 ]
        self.kp_weight = [ -750, -250, -50, -50 ]
        self.kp1 = -250
        self.kp = self.kp_weight
        ## The runs of this finite state machine
        self.runs = 0
        ##the previous time of the system
        self.previousTime = time.ticks_us()
        ##previous x
        self.previousX = 0
        ##previous y
        self.previousY = 0
        ##previous theta x
        self.previousThetaX = 0
        ##previous theta y
        self.previousThetaY = 0
        ##previous theta x dot 
        self.previousThetaXdot = 0
        ##previous theta y dot
        self.previousThetaYdot = 0
        self.gain = 0
        self.index = 0
        
    def run(self):
        """
        @brief          This function will compute the actuation value
        @details        This function will allow the computation and return of 
                        updated actuation value. This value will be based on 
                        the measured and reference values.        
                        Motor notes
        
                        Motor 1 negative theta_y relation
                        
                        x position - max 88
                        x velocity - 
                        y angle - max 330
                        y angular velocity - 400
                        
                        
                        Motor 2 positive theata_x relation
                        
                        Motor 2 needs 
                        y position - max 50
                        y velocity
                        x angle - max 300
                        x angular velocity - 400   
        @param ref      The ref measured of the system. 
        @param meas     The omega measured of the system.
        @return         The duty of the function.
        """
        # updating the angle values
        self.theta_y = self.roll.read()
        self.theta_x= self.pitch.read()
        #print(self.theta_x, 'x', self.theta_y, 'y')
        # updating x and y values
        self.Z = self.zcord.read()
        if self.Z == True:
            self.X = self.xcord.read()
            self.Y = self.ycord.read()
        else:
            self.X = 0
            self.Y = 0
        
        
        # updating theta dot values
        self.theta_xdot = self.pitch_vel.read()
        self.theta_ydot = self.roll_vel.read()
        
        ##delta time and updating previous to current
        self.currentTime = time.ticks_us()
        self.deltaTime = time.ticks_diff(self.currentTime, self.previousTime)
        self.previousTime = self.currentTime
        
        ## calculating delta x
        self.Xdot = (self.X - self.previousX)/self.deltaTime
        self.previousX = self.X
        
        ##calculating delta y
        self.Ydot = (self.Y - self.previousY)/self.deltaTime
        self.previousY = self.Y
        
        #print(self.Xdot, 'x', self.Ydot, 'y')
        #print(self.theta_xdot, 'xdot', self.theta_ydot, 'ydot')
      
        
        # speed controller 
        if self.Flag.read() == 2:
            if self.system == 1: 
                self.term_1 = -self.kp[0]*self.X
                self.term_2 = -self.kp[1]*self.theta_y
                self.term_3 = -self.kp[2]*self.Xdot
                self.term_4 = -self.kp[3]*self.theta_ydot
               
                self.gain_0 = self.term_1 + self.term_2 + self.term_3 + self.term_4
                self.gain = self.gain_0/1000
                
            elif self.system == 2:
                self.term_1 = self.kp[0]*self.Y
                self.term_2 = self.kp[1]*self.theta_x
                self.term_3 = self.kp[2]*self.Ydot
                self.term_4 = self.kp[3]*self.theta_xdot
               
                self.gain_0 = self.term_1 + self.term_2 + self.term_3 + self.term_4
                self.gain = self.gain_0/1000
           
            if self.gain > 17:
                self.gain = 30
            elif self.gain < -17:
                self.gain = -30
            else: 
                self.gain = 0
                
            if self.theta_y >= 100 and self.system == 1:
                self.gain = 30
                
            elif self.theta_x >= 100 and self.system == 2:
                self.gain = -30
                
            if self.theta_y <= -100 and self.system == 1:
                self.gain = -30
                
            elif self.theta_x <= -100 and self.system == 2:
                self.gain = 30
                
        if self.Flag.read() == 1:   
            if self.system == 1: 
               self.term_2 = -self.kp1*self.theta_y
               self.gain = self.term_2/1000
            
            elif self.system == 2:
               self.term_2 = self.kp1*self.theta_x
               self.gain = self.term_2 /1000
               
            if self.gain >= 50:
                self.gain = 50
                
            elif self.gain <= -50:
                self.gain = -50
        
            if abs(self.theta_y) >= 300:
                self.gain = 0
                
            elif abs(self.theta_x) >= 300:
                self.gain = 0
            
        self.motor.set_duty(self.gain)
            
        # if self.runs == 0 and self.system == 2:
        #     print( self.theta_y, self.theta_x, self.system, 'before')
        #     self.motor_drive.enable()
        #     self.motor.set_duty(50)
        #     utime.sleep(.1)
        #     self.motor.set_duty(0)
        # if self.runs == 1:
        #     print( self.theta_y, self.theta_x, self.system, 'after')
        # self.currentTime = time.ticks_us()
        # self.deltaTime = time.ticks_diff(self.previousTime, self.currentTime)/10000     
        # self.previousTime = self.currentTime
        
        
        # #print(self.desiredOmega, self.currentOmega)
        # self.desired_duty = self.currentkp*(self.desiredspeed - self.currentspeed)
        # if self.desired_duty > 100:
        #     self.duty = 100
        # elif self.desired_duty < -100:
        #     self.duty = -100
        # else:
        #     self.duty = self.desired_duty
        # #print(self.desired_duty, self.duty)
        # return self.duty
        self.runs += 1
        #utime.sleep(1)