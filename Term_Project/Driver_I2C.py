# -*- coding: utf-8 -*-
"""
@package    Term_Project
@file       Driver_I2C.py
@brief      This file will interact with a BNO055 Sensor and acts as a driver
@details    This file interacts with a BNO055 Sensor in order to calibrate the
            sensor and from there either change the operating mode, read the
            euler angle data, or read the angular velocity data. It will 
            create a class object called I2C_Driver that can be used by other 
            files in order to work with the IMU.
@author     James Lavelle Wyatt Conner
@date       8 December 2021
"""

# the import of struct
import struct

class I2C_Driver:
    
    def __init__(self, i2c, address):
        """
        @brief          This function will create an i2c object
        @details        This function will establish an i2c object that will 
                        work throughout the class. It will use the parameters 
                        of the i2c and the address in order to create and 
                        initialize this class.
        @param i2c      The i2c object that will be associated with this class.
        @param address  The address of the I2C that will be used by the file.
        """
        
        ## the address of the i2c
        self.address = address
        ## the i2c that will be interacted with
        self.i2c = i2c

    def set_operating_mode(self, mode):
        """
        @brief      This function will set the mode
        @details    This function will set the mode to NDOF at the number 12.
        @param mode The mode that the driver will need to be set at.
        """
        
        self.i2c.mem_write(mode, 0x28, 0x3D)
        
    def get_cal_status(self):
        """
        @brief      This will read data of calibration status of I2C unit
        @details    Depending upon the data read from the calibration status
                    we can determine if the system has been properly 
                    calibrated. It will use buffers, arrays, and bytes in 
                    order to achieve this.
        @return     The calibration status of the IMU.
        """
        
        ## the buffer used for calibration
        buf_cal = bytearray(1)  
        ## the calibration bytes of the system
        cal_bytes = self.i2c.mem_read(buf_cal, 0x28, 0x35) 
        ## the calibration status of the system
        cal_status = (cal_bytes[0] & 0b11,
              (cal_bytes[0] & 0b11 << 2) >> 2,
              (cal_bytes[0] & 0b11 << 4) >> 4,
              (cal_bytes[0] & 0b11 << 6) >> 6)
        
        return cal_status   
        
    def get_cal_coef(self):
        """
        @brief      This will return our calibration coefficents to allow us 
                    to properly read data
        @details    This allows for us read the calibration offsets for each 
                    sensor and will return the data in an array format. It 
                    will use a buffer and a byte array to meet this goal and 
                    will return a tuple.
        @return     This will return a tuple of the calibration coefficents. 
        """
        
        ## the buffer used for the byte array
        buf = bytearray(22)
        self.i2c.mem_read(buf, 0x28, 0x55)
        return tuple(struct.unpack('<hhhhhhhhhhh',buf))
        
    def write_cal_coef(self, cal_coef):
        """
        @brief      This will write to the calibration offsets
        @details    This will allow us to write to the calibration coefficents
                    to set them to what we want, as we can save the last 
                    calibration status and use them again once we reset the 
                    system.
        @param      cal_coef  this is 18 byte input that will set the values 
                    of the calibration coefficents
                    
        """
        
        # writing the calibration coefficient
        self.i2c.mem_write(cal_coef, 0x28, 0x55)
        pass
        
    def read_euler(self):
        """
        @brief      This will read the euler angle based off the I2C unit
        @details    This will read the euler angle data from the I2C unit.
                    It will read the data for all 3 axis of rotation, pitch
                    yaw and roll in order to return the euler angle from the 
                    IMU.
        @return     The euler angle of the IMU will be returned.
        """
        
        ## the buffer of the euler values
        self.buf_euler = bytearray(6)
        self.i2c.mem_read(self.buf_euler, 0x28, 0x1a)
        ## the euler angle of the IMU
        self.euler_ang = struct.unpack('hhh', self.buf_euler)
        return self.euler_ang

    def read_angular(self):
        """
        @brief      This will read angualr veolicty data from the I2C unit
        @details    This will read the angular velocity data from the I2C unit
                    for all 3 axis of rotation, the output is a 3 number array.
                    It will return the angular velocity of the IMU.
        @return     The angular velocity of the IMU will be returned.
                    
        """
        
        ## the buffer angle of the IMU
        self.buf_ang = bytearray(6)
        self.i2c.mem_read(self.buf_ang, 0x28, 0x14)
        ## the angular velocity of the IMU
        self.angular_vel = struct.unpack('hhh', self.buf_ang)
        return self.angular_vel