# -*- coding: utf-8 -*-
"""
@package            Term_Project
@file               Task_Data_Collection.py
@brief              This file will create a data collection task for the ball
                    ball balancing system.
@details            This file will construct a data collection task. It will 
                    run the update RT and update IMU functions in order to 
                    collect data. This data collection will run similarly to 
                    the driver touch and update these x, y, and z values based 
                    on the coefficients. Additionally, it will update the IMU 
                    values based on their calibrations.
@author             James Lavelle
@author             Wyatt Conner
@date               8 December 2021
"""

# the import of utime and the shares file
import utime
from shares import Share

class Data_Collection:
    """
    @brief      This function will create a data collection class object for 
                the ball balancing system.
    @details    This function will create a data collection class object for 
                the ball balancing system by updating the IMU and RT values 
                with the correct calibration coefficients. It will then 
                constantly run the system.
    """
    
    def __init__ (self, Flag, xcord, ycord, zcord, pitch, roll, pitch_vel, roll_vel, IMU, touch, cal_task):
        """
        @brief              This function will create a data collection class 
                            object for the ball balancing system.
        @details            This function will create a data collection class 
                            object for the ball balancing system by 
                            initializing all of the necessary objects shared 
                            from other files.
        @param Flag         The flag variable used throughout the program.
        @param xcord        The x coordinate values found from the touch pad.
        @param ycord        The y coordinate values found from the touch pad.
        @param zcord        The z coordinate values found from the touch pad.
        @param pitch        The pitch of the program found from the IMU.
        @param roll         The roll of the program found from the IMU.
        @param pitch_vel    The pitch velocity of the program from the IMU.
        @param roll_vel     The roll velocity of the program from the IMU.
        @param IMU          The IMU board used to calibrate the program.
        @param touch        The touch board used to balance the ball.
        @param cal_task     The calibration task of the program.
        """
        
        ## the shared flag 
        self.Flag = Flag
        ## the xcord of the touch
        self.xcord = xcord
        ## the ycord of the touch
        self.ycord = ycord
        ## z cord of the touch
        self.zcord = zcord
        ## the pitch of the IMU
        self.pitch = pitch
        ## the roll of the IMU
        self.roll = roll
        ## the pitch velocity of the IMU
        self.pitch_vel = pitch_vel
        ## the roll velocity of the IMU
        self.roll_vel = roll_vel
        ## the IMU
        self.IMU = IMU
        ## the touch panel
        self.touch = touch
        ## the calibration task for the ball balancing project
        self.cal_task = cal_task
        
    def run(self):
        """
        @brief      This function will update the x, y, and z values.
        @details    This function will update the x, y, and z values by using
                    the RT coefficient and multiplying that by the respective
                    coordinate values to find the new corrected value. 
                    Additionally, this function will also work with the pitch, 
                    roll, euler, and angular data from the IMU and update it 
                    based on the necessary coefficient values found from 
                    testing. 
        """
        # if the ball is touching then update the x and y values
        ## the z scan of the touch pad
        self.zscan = self.touch.scan_z()
        ## the y scan of the touch pad
        self.yscan = self.touch.scan_y()
        ## the x scan of the touch pad
        self.xscan = self.touch.scan_x()
        
        ## the RT_coef is the touch pad array of coefficients
        self.RT_coef = self.cal_task.get_k_RT()
        ## the y_correct is the correctly calibrated y value
        self.y_correct = self.yscan*self.RT_coef[1] - self.RT_coef[5]
        ## the x_correst is the correctly calibrated x value
        self.x_correct = self.xscan*self.RT_coef[0] - self.RT_coef[4]
            
        self.ycord.write(self.y_correct)
        self.xcord.write(self.x_correct)
        self.zcord.write(self.zscan)

        ## the mode is the correct operating mode for this function
        self.mode = 12
        self.IMU.set_operating_mode(self.mode)
        ## the euler is the euler values from the IMU
        self.Euler = self.IMU.read_euler()
        #3 the angular is the angular values from the IMU
        self.angular = self.IMU.read_angular()
        
        self.pitch.write(int(self.Euler[1]))
        self.roll.write(int(self.Euler[2]))
        
        self.pitch_vel.write(int(self.angular[1]))
        self.roll_vel.write(int(self.angular[2]))
        
        #print( self.Euler, 'angles', self.angular, 'velocity')
        
        
