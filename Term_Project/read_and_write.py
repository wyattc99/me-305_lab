# -*- coding: utf-8 -*-
"""
@package    Term_Project
@file       read_and_write.py
@brief      This file will read a text file and write a new one
@details    This file will create a class object that can read a file and then
            create a new one by writing it. The class object will change the 
            contents of the first file and convert them to a usable data type
            so that the correctly calibrated coeffients may be used by the 
            data collection task file.
@author     Wyatt Conner
@author     James Lavelle
@date       8 December 2021
"""

# imports of the python built in functions
import os
import time
import struct

class Read_Write_Text:
    """
    @brief
    @details    This function will create a class object that can read a file 
                and then create a new one by writing it. The class object will 
                change the contents of the first file and convert them to a 
                usable data type so that the correctly calibrated coeffients 
                may be used by the data collection task file.
    """
    
    # convert the standard notation into a byte array
    def read_hex(self, filename):
        """
        @brief              This function will read hex numbers from a file.
        @details            This function will read hex numbers from a file by
                            taking the file and creating an index. Then it 
                            will read the data from within the file.
        @param   filename   This is the filename.
        @return             This will return a new data file.
        """
        ## Index is used to keep track of what indice the program is on
        self.index = 0
        ## Data_hex is a variable used to store the data as it is being converted
        self.data_hex = []
        ## data is the inital data we read from the file
        self.data = []
        ## this defines the file's name which is an input
        self.filename = filename
        if self.filename in os.listdir():
            with open(self.filename, 'r') as f:
                self.data = f.readline()
                self.data = (self.data.strip().split(','))
                self.length = len(self.data)
                
                
            for self.index in range(self.length):
                self.hex_digit = int(self.data[self.index], 16)
                self.data_hex.append(self.hex_digit)
                #print(self.data_hex[self.index])
                self.index += 1
            self.output = bytearray(self.data_hex)
            return(self.output)
        else:
            print(self.filename,'not present to read. \n')
             
    def write_hex(self, filename, byte_array):
        """
        @brief              This function will write a hex value from a file.
        @details            This function will write a hex value from a given
                            file. It will use a given file to retrieve the 
                            data from it and then format it to write a new 
                            file in the form of hex numbers.
        @param   filename   This is the filename.
        @param   array      This is the array being used.
        @return             This function will return a hex of the data.
        """
        
        self.filename = filename
        self.data = byte_array
        self.data = ''.join(map(str,self.data))
        #self.data = byte_array.strip('bytearray','b','()').split('\\')
        with open(self.filename, 'w') as f:
            f.write(self.data)
       
        return(self.data)
       
    # convert interger string to array of values
    def read_int(self, filename):
        """
        @brief              This function will read integer data.
        @details            This function will read integer data from a file
                            and use this data to print a print statement. This
                            statement will include the data.
        @param   filename   This is the filename.
        @return             This function will return the read integers.
        """
        
        self.filename = filename
        if self.filename in os.listdir():
            with open(self.filename, 'r') as f:
                self.data = f.readline()
                self.data = [float(self.data) for self.data in self.data.strip().split(',')]
        else:
            print(self.filename, 'no present to read. \n')
        return(self.data)
    
    def write_int(self, filename, array):
        """
        @brief              This function will write an integer file.
        @details            This function will write an interger file by using
                            built in programs from python. It will also do 
                            this by using an array.
        @param   filename   This is the filename.
        @param   array      This is the array.
        """

        self.filename = filename
        self.data = array
        self.data = str(self.data).strip('[]')
        
        with open(self.filename, 'w') as f:
            f.write(self.data)
            