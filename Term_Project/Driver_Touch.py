# -*- coding: utf-8 -*-
"""
@package            Term_Project
@file               Driver_Touch.py
@brief              This file will construct a driver for the balancing system
@details            This file will construct a driver class that includes a
                    set of four input pins and a set of functions that will be 
                    able to determine the x values for the horizontal location
                    of the ball, the y values for the vertical location of the
                    ball, and lastly the z values for if the ball is in 
                    contact with the touch pad. Additionally, this driver 
                    should account for the settling time, and create a method
                    for returning these values as a tuple.
@author             James Lavelle
@author             Wyatt Conner
@date               8 December 2021
"""

# import of pyb, micropython, and utime
import pyb
import micropython
import utime

#the four location pin values used for the touch screen
#pin1 = xPpin = pyb.Pin(pyb.Pin.cpu.A7)
#pin2 = xMpin = pyb.Pin(pyb.Pin.cpu.A1)
#pin3 = yPpin = pyb.Pin(pyb.Pin.cpu.A6)
#pin4 = yMpin = pyb.Pin(pyb.Pin.cpu.A0)

class Touch_Driver():
    """
    @brief      This class will construct a touch driver class
    @details    This class will construct a touch driver class that will be 
                utilized in order to interact with task files that can locate 
                the x, y, and z positions of a touch pad. 
    """
    
    def __init__(self):
        """
        @brief              This function will define the touch driver class
        @details            This function will define the touch driver class 
                            by creating four pin values that will be utilized
                            later throughout the task files. Additionally, the
                            four pin values need a center value for 
                            calibration. This center value is given at 88 and 
                            50 mm respectively in the x and y directions.
        """ 
        
        #self.width = range(0, 176)
        #self.length = range(0, 100)
        ##the center of the touch panel
        self.center = (88,50)
        ## the xp pin of the touch pad
        self.pin1 = pyb.Pin.cpu.A7
        ## the xm pin of the touch pad
        self.pin2 = pyb.Pin.cpu.A1
        ## the yp pin of the touch pad
        self.pin3 = pyb.Pin.cpu.A6
        ## the ym pin of the touch pad
        self.pin4 = pyb.Pin.cpu.A0
        
    def scan_x(self):
        """
        @brief      This function will return the x coordinate of the object
        @details    This function will find the x coordinate of the object on 
                    the touch pad by calculating the numerical value of it's 
                    position. If it is located at the center it will be at 
                    location zero.
        @return     The x coordinate of the object on the touch pad.
        """
        
        #need to delay the start time of the data collection by more than 
        #3.6us in order to wait for the settling time
        #set xp high, not sure if we set = to 100 or just say high and 
        self.pin1.high()
        #set xm low
        self.pin2.low()
        #float yp
        pyb.Pin(self.pin3, pyb.Pin.IN)
        #measure ym to find x cord
        pyb.Pin(self.pin1, pyb.Pin.OUT_PP)
        pyb.Pin(self.pin2, pyb.Pin.OUT_PP)
        ## the adcx buffer used
        adcx = pyb.ADC(self.pin4)
        ## the buffered adc x values
        x_adc = adcx.read()
        ## the x coordinates of the system
        x_cord = x_adc*(176/4095) - self.center[0]
        
        return x_cord
    
    
    def scan_y(self):
        """
        @brief      This function will find the y coordinate of the object
        @details    This function will find the y coordinate of the object on 
                    the touch pad by calculating the numerical value of it's 
                    position. If it is located at the center it will be at 
                    location zero.
        @return     The y coordinate of the object on the touch pad.
        """
        
        #need to delay the start time of the data collection by more than 
        #3.6us in order to wait for the settling time
        #set yp high
        self.pin3.high()
        #set ym low
        self.pin4.low()
        #float xp
        pyb.Pin(self.pin1, pyb.Pin.IN)
        #measure xm to find y cord
        pyb.Pin(self.pin3, pyb.Pin.OUT_PP)
        pyb.Pin(self.pin4, pyb.Pin.OUT_PP)
        ## the adc y buffer of the system
        adcy = pyb.ADC(self.pin2)
        ## the adc y values of the touch pad
        y_adc = adcy.read()
        ## the correct y values of the touch pad
        y_cord = y_adc*(100/4095) - self.center[1]
        
        return y_cord
    
    def scan_z(self):
        """
        @brief      This function will find the z coordinate of the object
        @details    This function will find the z coordinate of the object on 
                    the touch pad by using a Boolean expression. If the object
                    is touching it will be true and false if not. 
        @return     The z coordinate of the object on the touch pad.
        """
        
        #need to delay the start time of the data collection by more than 
        #3.6us in order to wait for the settling time
        #set yp high
        self.pin3.high()
        #set xm low
        self.pin2.low()
        
        #meausre ym to find z cord
        pyb.Pin(self.pin3, pyb.Pin.OUT_PP)
        pyb.Pin(self.pin2, pyb.Pin.OUT_PP)
        ## the adc z buffer of the system
        adcz = pyb.ADC(self.pin4)
        ## the z coordinate values of the touch pad
        z_cord = adcz.read()

        if z_cord <= 4000:
            ## z flag of the touch pad
            z_flag = True
            return z_flag
        else:
            z_flag = False
            return z_flag
        
    def coordinate(self, x_cord, y_cord, z_cord):
        """
        @brief          This function will return the tuple of the coordinates
        @details        This function will return a tuple of the 3 coordinates.
                        These coordinates are the x, y, and z values found in 
                        the previous functions. 
        @param  x_cord  The x coordinate of the object on the touch pad.
        @param  y_cord  The y coordinate of the object on the touch pad.
        @param  z_cord  The z coordinate of the object on the touch pad.
        @return         The tuple of the three x, y, and z coordinates of the 
                        system.
        """
        
        #Taking all three of the coordinates and organize them into a tuple
        ## the tuple of all three coordinates of the touch pad
        tuple_cords = (x_cord, y_cord, z_cord)
        return tuple_cords