# -*- coding: utf-8 -*-
"""
@package    Term_Project

@file       Task_User.py
@brief      User interface task for ball balancing system.
@details    Implements a finite state machine for the user to input commands
            that can collect data, begin ball balancing, clear a fault, print 
            data or transition back to state 1. This user task will cooperate 
            with the term project package to utilize all other files.
@author     Wyatt Conner 
@author     James Lavelle
@date       8 December 2021
"""

# general imports from python
import utime
import pyb
from micropython import const

## State 0 Display Welcome Message
S0_INIT = const(0)
## State 1 Wait for User Input
S1_WAIT_FOR_CHAR = const(1)
## State 2 Gather Data
S2_GATHER_DATA = const(2)
## State 3 Clear Fault
S3_CLEAR_FAULT = const(3)
## State 4 Balance Ball
S4_BALANCE_BALL = const(4)
## State 5 Print Data to putty
S5_PRINT_DATA = const(5)

class user_task(): 
     """
         @brief      User interface task for ball balancing system.
         @details    Constructs a user interface task class that will be able 
                     to input commands that can collect data, begin ball 
                     balancing, clear a fault, print data or transition back 
                     to state 1. This user task will cooperate with the term 
                     project package to utilize all other files.
     """
    
     def __init__(self, Flag, xcord, ycord, zcord, pitch, roll, roll_vel, pitch_vel, Motor_Drive):
        """
            @brief              Constructs a user task for the ball balancer.
            @details            This function will construct and initialize 
                                the necessary parameters needed for the user
                                task of the ball balancing system.
            @param Flag         The flag used throughout the system.                      
            @param xcord        A shares.Share object used to hold the x data.
            @param ycord        A shares.Share object used to hold the y data.
            @param zcord        The z coordinate values found from the touch pad.
            @param pitch        The pitch of the program found from the IMU.
            @param roll         The roll of the program found from the IMU.
            @param pitch_vel    The pitch velocity of the program from the IMU.
            @param roll_vel     The roll velocity of the program from the IMU.
            @param Motor_Drive  The motor drive object from the driver file.
        """
        
        ## X data of the motor that will be shared
        self.xcord = xcord
        ## Y data of the motor represents the speed of motor
        self.ycord = ycord
        ## Z data of the touch
        self.zcord = zcord
        ## The flag variable share
        self.Flag = Flag
        ## The pitch of the IMU
        self.pitch = pitch
        ## The roll of the IMU
        self.roll = roll
        ## The pitch velocity of the IMU
        self.pitch_vel = pitch_vel
        ## The roll velocity of the IMU
        self.roll_vel = roll_vel
        ## the motor drive
        self.Motor_Drive = Motor_Drive
        
        # Define internal objects
        #
        ## The number of runs of the state machine
        self.runs = 0
        ## the number of times enabled is pushed
        self.enable_runs = 0
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The utime.ticks_us() value associated with the next run of the FSM
        ## Index number used to index each point of data collected
        self.index = 0
        ## The x data collected
        self.x_data = []
        ## The y collected
        self.y_data = []
        ## State of this finite state machine
        self.state = S0_INIT
        ## Start time of the finite state machine
        
        ## Ball balance parameter
        self.balance = 0
        ## Enable motor parameter
        self.enable_runs = 0
     def run(self):
            """
                @brief      Runs one iteration of the FSM
                @details    This function will run one iteration of the FSM in
                            order to perform the function that the user has 
                            entered into the PuTTY window. There are a number 
                            of command keys that will be chosen from by the 
                            user and each key will run a different state.
            """
            
            ## Start time of gathering data

#            if self.Flag.read() == 'User':
            if self.state == S0_INIT:
                print('Welcome to Ball Balancer')
                # print('\'d\' to start data collection')
                # print('\'b\' to start balancing ball')
                # print('\'c\' to clear motor fault')
                # print('\'p\' to print data')
                # print('\'h\' print command instructions')
                
                self.state = S1_WAIT_FOR_CHAR
                
            elif self.state == S1_WAIT_FOR_CHAR:
               if self.ser.any():
                   ## The character input by the user 
                   char_in = self.ser.read(1).decode()
                   
                   # enable motors
                   if(char_in == 'E' or char_in =='e'):
                       self.enable_runs += 1
                       if self.balance % 2 == 0:
                           self.Motor_Drive.disable()
                       else:
                           self.Motor_Drive.enable()
                       
                       
                   # Zero the Platform initally then ball 
                   if(char_in == 'B' or char_in =='b'):
                       self.balance += 1 
                       if self.balance % 2 == 0:
                           self.Flag.write(2)
                       else:
                           self.Flag.write(1)
                    
                    