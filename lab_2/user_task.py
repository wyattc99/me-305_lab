
''' 
@package    Lab_0x02


@file       task_user.py
@brief      User interface task for cooperative multitasking example.
@details    Implements a finite state machine for the user task to ask the user
            what information they want from the encoder. 
@author     Wyatt Conner 
@author     James lavelle
@date       October 20, 2021
'''

import utime, pyb
from micropython import const
#from task_encoder import encoder_task

## State 0 of the user interface task
S0_INIT             = const(0)
## State 1 of the user interface task
S1_WAIT_FOR_CHAR    = const(1)
## State 2 Gather Data for 30 seconds
S2_GATHER_DATA      = const(2)
## State 3 Print Data to putty
S3_PRINT_DATA       = const(3)


class task_user(): 
     ''' 
         @brief      User interface task for cooperative multitasking example.
         @details    Implements a finite state machine
     '''
    
     def __init__(self, flag, period, POS_share, delta_share):
        '''
            @brief              Constructs an user task.
            @details            The user task is to interact with the user and
                                output data that is desired by the user.
            @param flag         The flag of the task to zero encoder
            @param period       The period to set the frequency that the code 
                                will run at
            @param POS_share    A shares.Share object used to hold the Position
                                brightness.
            @param delta_share  A shares.Share object used to hole the delta.
        '''
        #  Share varibles between files in the folder
        #
        ## The name of the task
        self.flag = flag
        ## A shares.Share object representing Encoder period
        self.period = period
        ## A shares.Share object representing Encoder Position
        self.POS_share = POS_share
        ## A shares.Share object representing Encoder Delta
        self.delta_share = delta_share
        ## State of the finite state machine
        self.state = S0_INIT
        ## The number of runs of the state machine
        self.runs = 0
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## Index number used to index each point of data collected
        self.index = 0
        ## The position data collected
        self.position_data = []
        ## The time data collected
        self.time_data = []
      
     def run(self):
            '''
                @brief Runs one iteration of the FSM
            '''
            ## Start time of gathering data
            self.present_time = utime.ticks_ms()/1000
            if self.state == S0_INIT:
                print('Welcome, press \'z\' to zero encoder')
                print('\'p\' to print the encoder position.')
                print('\'d\' to print the detla of the encoder')
                print('\'g\'Collect enocder data for 30 seconds')
                print('\'s\' end data collection Permaturely')
                print('\'h\' print command instructions')
                
                self.state = S1_WAIT_FOR_CHAR
                
            elif self.state == S1_WAIT_FOR_CHAR:
               if self.ser.any():
                   ## The character input by the user 
                   char_in = self.ser.read(1).decode()
                    
                   if(char_in == 'z' or char_in == 'Z'):
                        ## Name of the task
                        self.flag.write(True)
                        print('Zeroing Position')
                     
                   elif(char_in == 'p' or char_in == 'P'):
                        print(self.POS_share.read())
                    
                   elif(char_in == 'd' or char_in == 'D'):
                        print(self.delta_share.read())
                       
                   elif(char_in == 'g' or char_in == 'G'):
                        self.state = S2_GATHER_DATA
                        self.print_data = True
                        print('Gatheirng Position Data . . .')
                            
                   elif(char_in == 'h' or char_in == 'h'):
                        self.state = S0_INIT
                              
            if self.state == S2_GATHER_DATA:
                ## start time is the time we start to take data
                self.start_time = utime.ticks_ms
                ## current time the iteration starts on
                self.current_time = utime.ticks_diff(self.present_time,self.start_time)
               
                utime.sleep_ms(100)
                self.position_data.append(str(self.POS_share.read()))
                self.time_data.append(self.current_time/1000)
                if self.current_time > 30:
                        self.state = S3_PRINT_DATA
                        self.start_time = 0
                        self.present_time =0
                        print('Data collection is complete.')
                elif self.ser.any():
                        char_in = self.ser.read(1).decode()
                        if(char_in == 's' or char_in == 'S'):
                            self.state = S3_PRINT_DATA
                            print('Data gathering has been halted.')  
            
           
            if self.state == S3_PRINT_DATA: 
                self.index += 1
                
                utime.sleep_ms(10)
                if self.index >= len(self.position_data):
                    self.state = S1_WAIT_FOR_CHAR
                    self.position_data.clear()
                    self.time_data.clear()
                    self.index = 0
                elif self.index <= len(self.position_data):
                     print(self.time_data[self.index], ',', self.position_data[self.index])
                                        
                    
                    