# -*- coding: utf-8 -*-
''' 
@package    Lab_0x02

@file encoder.py
@brief      A driver for reading from Quadrature Encoders
@details    A driver for the encoder to communicate with the hardware of the 
            encoder. It is capable of updating the position of the encoder and
            preventing overflow. It can also output the position and delta of
            the encoder. It can also set the position of the encoder. 
@author     Wyatt Conner
@author     James Lavelle
@date       October 20, 2021


'''
import pyb

class driver_encoder():
      '''   
      @brief    Interface with quadrature encoders
      @details  Class that involves the driver for the encoder. It includes
                functions to get the position, and delta of the encoder.
      '''
      def __init__(self, pin1_enc, pin2_enc, Timer):
          '''
          @brief    Defining an encoder driver
          @details  A driver for encoders that communcates with the hardware.
          
          @param pin1_enc   Parameter that calls the first pin of the encoder
                            that matches with channel 1 of the pin.
          @param pin2_enc   Parameter that calls the first pin of the encoder
                            that matches with channel 2 of the pin.
          @param Timer      This will callout the corresponding timer that is
                            associated with the pins called out for the encoder.
          '''
          self.position = 0
          self.count = 0
          self.delta = 0
          
          ## Time object for the encoder
          self.tim = pyb.Timer(Timer, prescaler=0, period=65535)
          ## Time object for channel 1 of the encoder
          self.tim_ch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin=pin1_enc)
          ## Time object for channel 2 of the encoder
          self.tim_ch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin=pin2_enc)
    
   
    
          print('Creating encoder object')
          ## Channel 1 object that determines the pins and channel used
          self.ch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin= pin1_enc)
          ## Channel 1 object that determines the pins and channel used
          self.ch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin= pin2_enc)
          
      def update(self):
         ''' 
          @brief        Updates encoder position and delta
          @details      This updates the counter every time it is ran. This counter
                        has a 16 bit maximum count, therefore we also correct for
                        overflow by correcting the value which will be used to create
                        the true position of the encoder. 
         '''
         # define the initial parameters of the encoder.
         #
         ## The count of previous iteration
         self.previous_count = self.count
         ## Updating the count for this iteration
         self.count= self.tim.counter()
         ## Delta variabel is previous count - count
         self.delta = self.count - self.previous_count
     
   # update encoder position value for when overflow occur
     # If delta goes from 0 to 65535 we need to minus 65535
         if self.delta >= 65535/2:
                            self.delta =- 65535
        
         
     # If delta goes from 65535 to 0 we need to add 65535
         if self.delta <= -65535/2:
                            self.delta =+ 65535
         
         ## current position of the encoder   
         self.position = self.previous_count + self.delta
         

      print('Reading encoder count and updating position and delta values')

      def get_position(self):
            ''' 
        @brief     Returns encoder position
        @details   This simply will output the current encoder position that is created
                   from the update function. 
        @return    The position of the encoder shaft
            '''
     
            return self.position

      def set_position(self, POS, Speed): 
            ''' 
            @brief Sets        encoder position
            @details           This is used to set a datum of set the current position of the
                               encoder to some numeric value the user wishes to be. 
            @param position    The new position of the encoder shaft
            @param delta       The speed of the encoder by determining the change in 
                               position every period.
            '''
            self.position = 0
            self.position = POS
            self.previous_count = POS
            self.delta = Speed
    
            print('Driver Set Position enabled')

      def get_delta(self):
            ''' 
            @brief     Returns encoder delta
            @details   This function simply outputs the current delta value that is
                       created in the update function. 
            @return    The change in position of the encoder shaft
                       between    the two most recent updates
                   '''
     
            return self.delta
