# -*- coding: utf-8 -*-
"""
@package    Lab_0x02
@brief      Package enables user to access encoder position
@details     Encoder Configuration 
            This is a set of files with drivers, tasks and main file
            to configure a user interface that can obtain data of
            the encoder's position and delta. It is also capable of
            setting the position of the encoder to zero. Also collect
            position data from the encoder for 30 seconds. 
            This is a diagram of our finite state machine of each task and
            our task diagram of lab 02. 
            \image html lab_2_diagram.jpg width=50%
@author     Wyatt Conner
@author     James Lavelle
@date       October 20, 2021

@file       main.py
@brief      Main script for cooperative multitasking example.
@details    Implements cooperative multitasking using tasks implemented by
            finite state machines.
            
@author     Wyatt Conner
@author     James Lavelle
@date       October 20, 2021
"""
import shares
import user_task
import task_encoder

def main():
    ''' 
    @brief      The main program for encoder file
    @details    The the file that communicates with the user task and encoder task
                and runs them in an infinite loop until it is broken.
    '''
    ## A shares.Share object used by the tasks for sharing Position.
    POS_share = shares.Share(0)
    ## A shares.Share object used by the tasks for sharing delta.
    delta_share = shares.Share(0)
    ## A shares.Share object used by the tasks for flag
    flag = shares.Share(0)
    
    ## An Encoder Task object
    task1 = task_encoder.encoder_task(flag, 100, POS_share, delta_share)
    ## A user interface object
    task2 = user_task.task_user(flag, 100, POS_share, delta_share )
    ## list of tasks called in the main file
    task_list = [task1, task2]
    
    while(True):
        try:
            for task in task_list:
                task.run()
            
                
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
if __name__ == '__main__':
    '''
    @brief      Defining this file as the main file
    @details    setting up this file to be the main file of encoder file set. 
    '''
    main()
