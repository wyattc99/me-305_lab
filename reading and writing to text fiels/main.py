# -*- coding: utf-8 -*-
"""
Created on Mon Dec  6 16:39:40 2021

@author: wyatt
"""
from read_and_write import Read_Write_Text

if __name__ == '__main__':
    hex_data = Read_Write_Text().read_hex('IMU_cal_coeffs.txt')
    print(hex_data)
    
    int_data = Read_Write_Text().read_int('RT_cal_coeffs.txt')
    # print(int_data)
    
    Read_Write_Text().write_int('Int_test.txt', int_data)
    
    int_data = Read_Write_Text().read_int('Int_test.txt')
    print(int_data)
    
    hex_data = Read_Write_Text().write_int('Hex_test.txt', hex_data)
    print(hex_data)
    
    hex_data = Read_Write_Text().read_hex('Hex_test.txt')
    print(hex_data)