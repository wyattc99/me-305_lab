# -*- coding: utf-8 -*-
"""
Created on Mon Dec  6 16:17:41 2021

@author: wyatt
"""

import os
import time
import struct


class Read_Write_Text:
    
    # convert the standard notation into a byte array
    def read_hex(self, filename):
         ## Index is used to keep track of what indice the program is on
         self.index = 0
         ## Data_hex is a variable used to store the data as it is being converted
         self.data_hex = []
         ## data is the inital data we read from the file
         self.data = []
         ## this defines the file's name which is an input
         self.filename = filename
         if self.filename in os.listdir():
             with open(self.filename, 'r') as f:
                 self.data = f.readline()
                 self.data = (self.data.strip().split(','))
                 self.length = len(self.data)
                
                
             for self.index in range(self.length):
                 self.hex_digit = int(self.data[self.index], 16)
                 self.data_hex.append(self.hex_digit)
                #print(self.data_hex[self.index])
                 self.index += 1
             self.output = bytearray(self.data_hex)
             return(self.output)
         else:
             print(self.filename,'not present to read. \n')
             
    def write_hex(self, filename, byte_array):
       self.filename = filename
       self.data = byte_array.strip('bytearray','b','()').split('\\')
       with open(self.filename, 'w') as f:
            f.write(self.data)
       
       return(self.data)
       
        
        
    # convert interger string to array of values
    def read_int(self, filename):
        self.filename = filename
        if self.filename in os.listdir():
            with open(self.filename, 'r') as f:
                self.data = f.readline()
                self.data = [float(self.data) for self.data in self.data.strip().split(',')]
        else:
            print(self.filename, 'no present to read. \n')
        return(self.data)
    
    def write_int(self, filename, array):
        self.filename = filename
        self.data = array
        self.data = str(self.data).strip('[]')
        
        with open(self.filename, 'w') as f:
            f.write(self.data)
    
   
        
        
                 
             
         