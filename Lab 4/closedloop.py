# -*- coding: utf-8 -*-
"""
@file           closedloop.py
@brief          This file create a closed loop driver.
@details        This file will create a closed loop driver class that will
                allow a user to interface with a task file. This driver file 
                will allow for the setting of the gain and computation of 
                values. 
@author         James Lavelle
@author         Wyatt Conner
@date           18 November 2021
"""

import time

class Loop():
    """
    @brief      This class will create a closed loop controller
    @details    The closed loop controller class will create and construct a 
                closed loop controller that will allow the gain and pmw level 
                of the motor to be set so that the user task file can operate
                the system.
    """
    
    def __init__(self, kp):
        """
        @brief          Constructs the parameters for the closed loop class
        @details        This function will construct and initialize the 
                        parameters for the closed loop class. These parameters
                        include the gain and the pmw level.
        @param   kp     The gain of the system.
        @param   duty    The duty of the system. 
        """
        ##kp is the gain of the system
        self.updated_kp = kp
        ##the previous time of the system
        self.previousTime = time.ticks_us()
        ##the wanted omega value of the system
        self.omega
    
    def update(self, deltaPos):
        """
        @brief          This function will compute the actuation value
        @details        This function will allow the computation and return of 
                        updated actuation value. This value will be based on 
                        the measured and reference values. 
        @param ref      The ref measured of the system. 
        @param meas     The omega measured of the system.
        @return         The duty of the function.
        """
            
        self.currentTime = time.ticks_us()
        self.deltaTime = time.ticks_diff(self.previousTime, self.currentTime)/10000     
        self.previousTime = self.currentTime
        
        self.currentOmega = deltaPos
        
        #print(self.desiredOmega, self.currentOmega)
        self.desired_duty = self.currentkp*(self.desiredOmega - self.currentOmega)
        if self.desired_duty > 100:
            self.duty = 100
        elif self.desired_duty < -100:
            self.duty = -100
        else:
            self.duty = self.desired_duty
        #print(self.desired_duty, self.duty)
        return self.duty
            
    def set_kp(self, newkp):
        """
        @brief              This function will set the kp
        @details            This function will set the gain of the function by
                            setting the kp and the level to a set value. 
        @param   newkp      This param is the new gain of the system.
        """    
        
        self.currentkp = newkp
        
    def get_kp(self):
        """
        @brief      This function will return the gain of the system
        @details    This function will find and return the gain of the system
                    or kp as it is called.
        @return     The current gain or kp of the system will be returned.
        """
        
        return self.currentkp
    
    def set_omega(self, omega):
        """
        @brief          This function will set omega
        @details        This function will set the omega value of the system. 
                        It does this by 
        @param   omega  This is the omega of the function.
        """
        
        self.currentOmega = omega
        
    def get_omega(self):
        """
        @brief      This function will get the omega.
        @details    This function will get the current omega value of the 
                    system. This is the current current omega. 
        @return     The current omega value of the function will be returned.
        """
        
        return self.currentOmega
