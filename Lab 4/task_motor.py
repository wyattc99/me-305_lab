''' 
@package    Lab_0x03

@file       task_motor.py
@brief      motor task for cooperative multitasking example.
@details    Implements a finite state machine for the encoder task to 
            communicate with the encoder driver to update positions, delta
            and reset the position to zero.
@author     Wyatt Conner, James Lavelle
@date       October 29, 2021
'''
import utime, pyb
from micropython import const
from motordriver import Driver
from motordriver import Motor

class motor_task:
    ''' 
        @brief      Motor task that calls the Motor driver.
        @details    Implements a finite state machine that determines the position
                    delta and set the speed of the motor. 
    '''
    def __init__(self, sys_state, period, system, duty_share, Motor1, Motor2, Motordrive):
        ''' 
            @brief              Constructs an motor task.
            @details            The encoder task is implemented as a finite state
                                machine.
            @param sys_state    The state of the system
            @param period       determines the period of time this function will take
            @param system       determines what hardware set will be used
            @param duty_share   A shares.Share object used to hole the duty percent
            @param Motor1       An object for the motor 1 driver
            @param Motor2       An object for the motor 2 driver
            @param Motordrive   This is an object for the motor driver that handles operations for both motors. 
        '''
        # Setting up the share variables between tasks
        #
        ## System state to determine when enable certain function act
        self.sys_state = sys_state
        # state 0 - null no commans
        # state 1 - zero encoder 1
        # state 2 - zero encoder 2
        # state 3 - fault motors
        # state 4 - enable motors
        # state 5 - disable motors
        # state 6 - set duty cycle
        
        ## Period of MCU to run on
        self.period = period
        ## Duty to determine duty cycle percent provided to the motor
        self.duty_share = duty_share

        ## System is to define what system is being called 1 or 2
        self.system = system
        
        ## Run variable that represents how many times the task has ran
        self.runs = 0
        ## used to save the previous duty to see it changes
        self.previous_duty = 0
        ## used to save the current duty cycle
        self.duty = 0
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## Motor driver object for motor 1
        self.Motor1 = Motor1
        ## Motor driver object for motor 2
        self.Motor2 = Motor2
        ## Motor driver object for shared functions of motor 1 and 2
        self.Motordrive = Motordrive
     
    def run(self):
        ''' 
        @brief     Runs one iteration of the FSM
        @details   This function will run one the motor by using the driver motor class.
        '''
        self.runs += 1
       
        
        if self.runs == 1:
            print('starting motor task')
        
        if self.sys_state.read() == 4:
            self.Motordrive.enable() 
            self.sys_state.write(0)
        
        elif self.sys_state.read() == 5:
            self.Motordrive.disable()
            self.sys_state.write(0)
        
        elif self.sys_state.read() == 6: 
            print('duty has been inputed')
            
            if self.system.read() == 1:
                self.Motor1.set_duty(self.duty_share.read())
                print('motor 1 has been set')
                
            elif self.system.read() == 2:
                self.Motor2.set_duty(self.duty_share.read())
                print('motor 2 has been set')
        self.sys_state.write(0)
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
    
        
        