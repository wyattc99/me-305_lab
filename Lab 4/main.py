# -*- coding: utf-8 -*-
"""
<<<<<<< HEAD
@package    Lab_0x04
@brief      Main script for cooperative multitasking example.
@details    This lab is a speed controller of a motor. This is done by simple
            setting a wanted speed and measuring the speed of our motor currently.
            We take that difference and then multiply it by our gain to compute
            our new duty cycle to provide our motor. This means the motor will always
            be chaning duty cycle to try to get closer to the proper speed it
            was set to. To do this we implemented a closed loop system that will
            always update the value of speed of the motor from the encoders, then
            compute a new duty cyle to the provide the motors in which it will be
            given to the motor drivers to set the new duty. This loop is ran continously
            once prompted by the user. The user like in othe labs will be able to collect
            data for 10 seconds. 
            Task Diagram
            \image html Lab4_Taskdia.jpg width=50%
            State Transistion Diagram
            \image html Lab4_StateTran.jpg width=50%
            Flow Diagram
            \image html Lab4_Flow.jpg width=50%
            
@file       main.py
@brief      Main script for cooperative multitasking example.
@details    This lab is a speed controller of a motor. This is done by simple
            setting a wanted speed and measuring the speed of our motor currently.
            We take that difference and then multiply it by our gain to compute
            our new duty cycle to provide our motor. This means the motor will always
            be chaning duty cycle to try to get closer to the proper speed it
            was set to. To do this we implemented a closed loop system that will
            always update the value of speed of the motor from the encoders, then
            compute a new duty cyle to the provide the motors in which it will be
            given to the motor drivers to set the new duty. This loop is ran continously
            once prompted by the user. The user like in othe labs will be able to collect
            data for 10 seconds. 
            Task Diagram
            \image html Lab4_Taskdia.jpg width=50%
            State Transistion Diagram
            \image html Lab4_StateTran.jpg width=50%
            Flow Diagram
            \image html Lab4_Flow.jpg width=50%
            
@author     Wyatt Conner
@author     James Lavelle
@date       Novemeber 18, 2021
"""
import pyb
import time
import shares
import user_task
import task_encoder
import task_motor
import encoder
import motordriver

def main():
    ''' 
    @brief      The main program for encoder file
    @details    The the file that communicates with the user task and encoder task
                and runs them in an infinite loop until it is broken.
    '''
    ## A shares.Share object used by the tasks for sharing Position.
    POS1_share = shares.Share(0)
    ## A shares.Share object used by the tasks for sharing delta.
    delta1_share = shares.Share(0)
    ## A shares.Share object used by the tasks for sharing Position.
    POS2_share = shares.Share(0)
    ## A shares.Share object used by the tasks for sharing delta.
    delta2_share = shares.Share(0)
    ## A shares.Share object used by the tasks for flag
    sys_state = shares.Share(0)
     # state 0 - null no commans
     # state 1 - zero encoders
     # state 2 - fault motors
     # state 3 - enable motors
     # state 4 - disable motors
    ## A share.Share object used by tasks for tracking duty cycle
    duty_share = shares.Share(0)
    ## Creating a shared object to repersent system is being used
    system = shares.Share(0)
    
    Motordrive = motordriver.Driver()
    #period = 100
    ## create a motor object for motor 1
    Motor1 = motordriver.Motor(pyb.Pin (pyb.Pin.cpu.B4), pyb.Pin (pyb.Pin.cpu.B5), 1, 2, 3)
    
    ## create a motor object for motor 2
    Motor2 = motordriver.Motor(pyb.Pin (pyb.Pin.cpu.B0), pyb.Pin (pyb.Pin.cpu.B1), 3, 4, 3)
    
    ## creating encoder object for encoder 1
    Encoder1 = encoder.driver_encoder(pyb.Pin (pyb.Pin.cpu.B6),pyb.Pin (pyb.Pin.cpu.B6), 1, 2, 4)
    
    ## creating encoder object for encoder 2
    Encoder2 = encoder.driver_encoder(pyb.Pin (pyb.Pin.cpu.C6),pyb.Pin (pyb.Pin.cpu.C7), 3, 4, 8)
    
    ## An Encoder Task object
    task1 = task_encoder.encoder_task(sys_state, 100, system, POS1_share, delta1_share, POS2_share, delta2_share, Encoder1, Encoder2)
    ## A user interface object
    task2 = user_task.task_user(sys_state, 100, system,  duty_share, POS1_share, delta1_share, POS2_share, delta2_share)
    
    ## A motor task object
    task3 = task_motor.motor_task(sys_state, 100, system, duty_share, Motor1, Motor2, Motordrive)
    ## list of tasks called in the main file
    task_list = [task1, task2, task3]
    
            
    while(True):
        try:
            startTime = time.ticks_us()
           
            
            for task in task_list:
                task.run()
            
            stopTime = time.ticks_us()
            time.sleep(50000 - time.ticks_diff(stopTime,startTime)/1000000)
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
if __name__ == '__main__':
    '''
    @brief      Defining this file as the main file
    @details    setting up this file to be the main file of encoder file set. 
    '''
    main()
=======
@file               main.py
@brief              This file will act as the main for the closed loop driver
@details            This file will import the closed loop driver file as well 
                    as the task loop file in order to operate the motor. This 
                    will get the proper time, duty, and gain of the system.
@author             James Lavelle
@author             Wyatt Conner
@date               18 November 2021
"""

import shares
import time
import task_encoder
import closedloop
import taskloop
import motordriver

action_share = shares.Share(0)
duty_share = shares.Share(0)
gain_share = shares.Share(0)
deltaPosition_share = shares.Share(0)
setpoint_share = shares.Share(0)

user = taskloop.loop_task(action_share, duty_share, gain_share, setpoint_share)
encoder = task_encoder.encoder_task(action_share, deltaPosition_share)
motor = motordriver.Driver(action_share, duty_share, deltaPosition_share, gain_share, setpoint_share)
controller = closedloop.Loop(5, deltaPosition_share, gain_share, setpoint_share)

if __name__ == '__main__':
    while True:
        startTime = time.ticks_us()
        user.User_input()
        encoder.encoder_update()
        motor.controller_update()
        motor.motor_update()
        stopTime = time.ticks_us()
        time.sleep((50000 - time.ticks_diff(stopTime, startTime))/1000000)
>>>>>>> 46b7a1bb0cba2eae0c9814abfac0d870e1948947
