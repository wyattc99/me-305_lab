# -*- coding: utf-8 -*-
"""Timer.channel(channel, mode, ...)
@file:      Conner_305_0x01.py
@brief:     File enables a Nucleo to flash various signals on an LED. 
@author:    Wyatt Conner, James Lavelle
@date:      October 8, 2021
@details:  
This is the main and only file of the lab0x01 package. This code entierly runs
LED lightshow on the nucleo. This code acts as main file, task file and class file
by calling upon each pin and interacting with hardware and executing the actions
as needed to make the LED lightshow.
    
Image of our lab 01 state transistion diagram for Conner_305_0x01.py.
    
\image html Lab01_StateTran.jpg width=50%

A video of the code running can be seen here:
    
\htmlonly
<iframe width="560" height="315" 
src="https://www.youtube.com/embed/ykVqcsDESvo" 
title="YouTube video player" frameborder="0" 
allowfullscreen></iframe>
\endhtmlonly        
            
@package:   Lab_0x01
@brief:     File enables a Nucleo to flash various signals on an LED. 
@author:    Wyatt Conner, James Lavelle
@date:      October 8, 2021


"""

# Importing objects
import utime
import pyb
import math

##Button Variable is used to indicate wheter the button has been pressed or not.
button = 0

# Import object definitions
##pinA5 is a variable to define the pin being used for the LED.
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)

##pinC13 is a variable to define the pin being used for the button.
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

##tim2 is a variable that defines the timing in which the LED will operate on.
tim2 = pyb.Timer(2, freq = 20000)

##t2ch1 is a variable that sends power input to our pinA5 to light up the LED
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

# Function that says wheter the button is pushed or not
def onButtonPressFCN(IRQ_src):
    """
    @brief          Determines when Nucleo Button has been pushed
    @details        1.LED Flash Game
                        This is a single file that is used to play a flash game,
                        where pushing the button a nucleio will transition the
                        different LED patterns. The patterns are as follows, 
                        sawtooth, square and sin wave.
                    
                        This is our state transisiton diagram. 
                        \image html Lab01_StateTran.jpg width=50%
                        
                        A video of the code running can be seen here:
                        \htmlonly
                        <iframe width="560" height="315" 
                        src="https://www.youtube.com/embed/ykVqcsDESvo" 
                        title="YouTube video player" frameborder="0" 
                        allowfullscreen></iframe>
                        \endhtmlonly  
                    
    @param IRQ_src  This is an input variable retrieved from the nucleo that 
                    determines when the button is pressed. 
    @return         Set a global variable "button" to 1, indicating the button 
                    has been pushed. 
    """
    global button
    button = 1
    
##ButtonInt is a variable that determines when the button has been pressed.
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

# Function for saw tooth, square and sin waves 
def update_STW(Current_Time):
    """
    @brief          Produces a sawtooth wave pattern
    @details        The function creates a sawtooth wave pattern based upon the 
                    internal time of MCU, by outputing the time remainder divided
                    by one.
    @param Current_Time  The current time of the MCU, or how long the code has
                    been running. 
    @return         Will return a value from 0 to 100 depending upon the decimal
                    point of the time variable. 
    """
    
    return 100*(Current_Time%1.0)


def update_SQW(Current_Time):
    """
    @brief          Produces a square wave pattern
    @details        The function creates a square wave pattern based upon the 
                    internal time of MCU. The output is 1 if time remainder
                    is less than 0.5 and outputs 0 for greater than 0.5.
    @param Current_Time  The current time of the MCU, or how long the code has
                    been running. 
    @return         Will return a value of 0 or 100 depending upon the remainder
                    value of the current time parameter. 
    """
    
    return 100*(Current_Time%1.0<0.5)


def update_SIN(Current_Time):
    """
    @brief          Produces a Sin wave pattern
    @details        The function produces a sin wave based upon the MCU remainder
                    time so naturally the frequncy of the sin wave is one second.
                    The sin wave has an amplitude of 0.5 and is offset by 0.5,
                    so that no negative numbers are ouput. 
    @param Current_Time  The current time of the MCU, or how long the code has
                    been running. 
    @return         Will return a value from 0 to 100 in a sin wave pattern depending
                    upon the remainder of the current time parameter. 
    """
    
    return 100*(math.sin(Current_Time%1.0))


if __name__ == '__main__':

    ##State represents the numerical state the code is currently in.
    state = 0
    

    ##run determines keeps track of how many times the code has been ran.
    run = 0
    
    ##statTime determines the time the LED patterns begin.
    startTime = utime.ticks_ms()

    while(True):
        try:
            ##stopTime determines the current time of the iteration.
            stopTime= utime.ticks_ms()
            
            ##Current_Time determines how much time has passed since the startTime.
            Current_Time = (utime.ticks_diff(stopTime, startTime)/1000)
            
            if (state == 0):
                #run state code 0
                print('You are playing lED blinker. To transition between modes press the button.')
                state = 1               #transition to state 1
            
            elif (state == 1):
               #run state code 1
               if button == 1:
                   state = 2                #transition to state 2
                   button = 0
           
            elif (state == 2):
                #run state code 2
                t2ch1.pulse_width_percent(update_SQW(Current_Time))
                if button == 1:
                    state = 3               #transition to state 3
                    button = 0
            
            elif (state == 3):
                #run state code 3
                t2ch1.pulse_width_percent(update_SIN(Current_Time))
                if button == 1:
                    state = 4               #transition to state 4
                    button = 0
            
            elif (state == 4):
                #run state code 4
                t2ch1.pulse_width_percent(update_STW(Current_Time))
                if button == 1:
                    state = 2               #transition to state 2
                    button = 0
            
            run += 1                    #iteration
        
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')