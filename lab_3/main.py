# -*- coding: utf-8 -*-
"""
@package    Lab_0x03

@file       main.py
@brief      Main script for cooperative multitasking example.
@details    Implements cooperative multitasking using tasks implemented by
            finite state machines to operate encoder and motor system. This will
            allow users to control the duty cycle of the motors and gather data
            about postion and angualr velocity of the encoder. The following is data
            we got from a 30 second interval of data we collected from the encoders
            for angular displacement and velocity.
            Angular Displacement
            \image html Lab3_anglegraph.jpg width=50%
            Angular Velocity
            \image html Lab3_velocitygraph.jpg width=50%
            
@author     Wyatt Conner
@author     James Lavelle
@date       Novemeber 18, 2021
"""
import pyb
import time
import shares
import user_task
import task_encoder
import task_motor
import encoder
import motordriver

def main():
    ''' 
    @brief      The main program for encoder file
    @details    The the file that communicates with the user task and encoder task
                and runs them in an infinite loop until it is broken.
    '''
    ## A shares.Share object used by the tasks for sharing Position.
    POS1_share = shares.Share(0)
    ## A shares.Share object used by the tasks for sharing delta.
    delta1_share = shares.Share(0)
    ## A shares.Share object used by the tasks for sharing Position.
    POS2_share = shares.Share(0)
    ## A shares.Share object used by the tasks for sharing delta.
    delta2_share = shares.Share(0)
    ## A shares.Share object used by the tasks for flag
    sys_state = shares.Share(0)
     # state 0 - null no commans
     # state 1 - zero encoders
     # state 2 - fault motors
     # state 3 - enable motors
     # state 4 - disable motors
    ## A share.Share object used by tasks for tracking duty cycle
    duty_share = shares.Share(0)
    ## Creating a shared object to repersent system is being used
    system = shares.Share(0)
    
    Motordrive = motordriver.Driver()
    #period = 100
    ## create a motor object for motor 1
    Motor1 = motordriver.Motor(pyb.Pin (pyb.Pin.cpu.B4), pyb.Pin (pyb.Pin.cpu.B5), 1, 2, 3)
    
    ## create a motor object for motor 2
    Motor2 = motordriver.Motor(pyb.Pin (pyb.Pin.cpu.B0), pyb.Pin (pyb.Pin.cpu.B1), 3, 4, 3)
    
    ## creating encoder object for encoder 1
    Encoder1 = encoder.driver_encoder(pyb.Pin (pyb.Pin.cpu.B6),pyb.Pin (pyb.Pin.cpu.B6), 1, 2, 4)
    
    ## creating encoder object for encoder 2
    Encoder2 = encoder.driver_encoder(pyb.Pin (pyb.Pin.cpu.C6),pyb.Pin (pyb.Pin.cpu.C7), 3, 4, 8)
    
    ## An Encoder Task object
    task1 = task_encoder.encoder_task(sys_state, 100, system, POS1_share, delta1_share, POS2_share, delta2_share, Encoder1, Encoder2)
    ## A user interface object
    task2 = user_task.task_user(sys_state, 100, system,  duty_share, POS1_share, delta1_share, POS2_share, delta2_share)
    
    ## A motor task object
    task3 = task_motor.motor_task(sys_state, 100, system, duty_share, Motor1, Motor2, Motordrive)
    ## list of tasks called in the main file
    task_list = [task1, task2, task3]
    
            
    while(True):
        try:
            #startTime = time.ticks_us()
           
            
            for task in task_list:
                task.run()
            
            #stopTime = time.ticks_us()
            #time.sleep(50000 - time.ticks_diff(stopTime,startTime)/1000000)
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
if __name__ == '__main__':
    '''
    @brief      Defining this file as the main file
    @details    setting up this file to be the main file of encoder file set. 
    '''
    main()