
"""
Created on Thu Oct 21 15:37:10 2021

@author: wyatt
"""

import pyb
import shares
import task_motor

 #import pins for motor
pinB4 = pyb.Pin (pyb.Pin.cpu.B4)
pinB5 = pyb.Pin (pyb.Pin.cpu.B5)
pinA15 = pyb.Pin (pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)


 #import pin to enable motors
pinA15.high()

#setting up the Timer 3 object
tim3 = pyb.Timer(3, freq = 20000)

t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=pinB4)
t3ch1.pulse_width_percent(0)

t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=pinB5)
t3ch2.pulse_width_percent(0)
def main():
    ''' 
    @brief      The main program for encoder file
    @details    The the file that communicates with the user task and encoder task
                and runs them in an infinite loop until it is broken.
    '''
   
    ## A shares.Share object used by the tasks for sharing Position.
    POS1_share = shares.Share(0)
    POS2_share = shares.Share(0)
    delta1_share = shares.Share(0)
    delta2_share = shares.Share(0)
    ## A shares.Share object used by the tasks for sharing Position.
    sys_state = shares.Share(0)
    system = shares.Share(0)
    ## A shares.Share object used by the tasks for flag
    duty = shares.Share(0)
    
    system.write(1)
    sys_state.write(4)
    runs = 0
    
    task = task_motor.motor_task(sys_state, 100, system, duty, POS1_share, delta1_share, POS2_share, delta2_share)
    task_list = [task]
    while(True):
        try: 
            for task in task_list:
                task.run()
                runs +=1
                if runs == 8:
                    duty.write(100)
                    print('duty set')
                
                          
                
        except KeyboardInterrupt:
            'program terminating'
            break
if __name__ == '__main__':
    '''
    @brief      Defining this file as the main file
    @details    setting up this file to be the main file of encoder file set. 
    '''
    main()
        