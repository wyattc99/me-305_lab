''' 
@package    Lab_0x03


@file       task_encoder.py
@brief      Encoder task to output data from encoder driver.
@details    Implements a finite state machine for the encoder task to 
            communicate with the encoder driver to update positions, delta
            and reset the position to zero.
@author     Wyatt Conner 
@author     James Lavelle
@date       November 18, 2021
 
'''

import utime, pyb
from micropython import const
from encoder import driver_encoder


class encoder_task():
    ''' 
        @brief      Encoder task that calls the encoder driver.
        @details    Implements a finite state machine that determines the position
                    delta and set the position of the encoder. 
    '''
    
    def __init__(self, sys_state, period, system, POS1_share, 
                 delta1_share, POS2_share, delta2_share, Encoder1, Encoder2):
        ''' 
            @brief              Constructs an motor task.
            @details            The encoder task is implemented as a finite state
                                machine.
            @param sys_state    The state of the system
            @param period       determines the period of time this function will take
            @param system       determines what hardware set will be used
            @param duty_share   A shares.Share object used to hole the duty percent
            @param POS1_share   A shares.Share object used to hold the Position
                                of encoder 1.
            @param POS2_share   A shares.Share object used to hold the Position
                                of encoder 2.
            @param delta1_share     A shares.Share object used represent delta of
                                    encoder 1. 
            @param delta2_share     A shares.Share object used represent delta of
                                    encoder 2. 
            @param Encoder1     The object of the encoder 1 driver
            @param Encoder2     The oblject of the encoder 2 driver
        '''
        # Setting up the share variables between tasks
        #
        ## System state to determine when enable certain function act
        self.sys_state = sys_state
        # state 0 - null no commans
        # state 1 - zero encoder 1
        # state 2 - zero encoder 2
        # state 3 - fault motors
        # state 4 - enable motors
        # state 5 - disable motors
        # state 6 - set duty cycles
        
        ## Period of MCU to run on
        self.period = period
        ## Position of the motor that will be shared
        self.POS1_share = POS1_share
        ## Delta of the motor represents the speed of motor
        self.delta1_share = delta1_share
        ## Position of the motor that will be shared
        self.POS2_share = POS2_share
        ## Delta of the motor represents the speed of motor
        self.delta2_share = delta2_share
        ## System is to define what system is being called 1 or 2
        self.system = system
        ## Object of encoder 1 driver 
        self.Encoder1 = Encoder1
        ## Object of encoder 2 driver 
        self.Encoder2 = Encoder2
        
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.previousTime = utime.ticks_us()
        
        
    def run(self):
        ''' 
        @brief     Runs one iteration of the FSM
        @details   This function will run one the encoder by using the driver encoder class.
        '''
        ## determines the current time of the iteration
        self.currentTime = utime.ticks_us()
        ## Calculates the difference in time per iteration
        self.deltaT = utime.ticks_diff(self.previousTime, self.currentTime)/1000000
        ## Set the next previous time to the current time for next iteration
        self.previousTime = self.currentTime
        
        if self.sys_state.read() == 1 :
            self.Encoder1.set_position(0,0) 
            self.sys_state.write(0)
            print('zeroing enabled on encoder 1')
        elif self.sys_state.read() == 2 :
            self.Encoder2.set_position(0,0) 
            self.sys_state.write(0)
            print('zeroing enabled on encoder 2')
            
        # update encoder 1    
        self.Encoder1.update(1)
        self.POS1_share.write(self.Encoder1.get_position())
        
        self.delta1 = self.Encoder1.get_delta()*2*3.14/(2000*self.deltaT)
        self.delta1_share.write(self.delta1)

        
        # update encoder 2
        self.Encoder2.update(2)
        self.POS2_share.write(self.Encoder2.get_position())
        self.delta2 = self.Encoder2.get_delta()*2*3.14/(2000*self.deltaT)
        self.delta2_share.write(self.delta2)
           
        
       
          
               
               
            