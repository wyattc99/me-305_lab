# -*- coding: utf-8 -*-
"""
Created on Thu Nov 11 12:11:22 2021

@file       main.py
@brief      This file will interact with the driver file
@details    This file interacts with a BNO055 Sensor in order to calibrate the
            sensor and from there either change the operating mode, read the
            euler angle data, or read the angular velocity data.
            This is a video of our code operation. 
            
@author     James Lavelle Wyatt Conner
@date       11 November 2021
"""

from pyb import I2C
import classdriver
import utime

if __name__ == '__main__':
    
    i2c = I2C(1, I2C.MASTER)
    i2c.init(I2C.MASTER, baudrate = 500000)
    utime.sleep(1)
    
    address = 0x28
    controller = classdriver.driver(address, i2c)    
    mode = 12
    controller.set_operating_mode(mode)
    
    euler_cal = []
    
    status = False
    while (status == False):
        try:
            calibration_status = controller.get_cal_status()
            print ('Calibration Status Values:')
            print (calibration_status)
            utime.sleep(1)
            if (calibration_status[0] + calibration_status[1] + calibration_status[2] + calibration_status[3] == 12):
                print('Calibration Completed')
                print('Calibration Status:')
                mode = 0
                controller.set_operating_mode(mode)
                coef = controller.get_cal_coef()
                print(coef)
                mode = 12
                controller.set_operating_mode(mode)
                status = True
        
        except KeyboardInterrupt:
            break
        
    while (status == True):
        try:
            euler_stuff = controller.read_euler()
            angular_stuff = controller.read_angular()
            #euler_cal[0] = euler_stuff[0] - coef[0]
            #euler_cal[1] = euler_stuff[1] - coef[1]
            #euler_cal[2] = euler_stuff[2] - coef[2]
            print('Calibrated Euler Angles[Degrees]:', euler_stuff)
            print('Calibrated Angular Velocity [Degrees/sec]:', angular_stuff)
            print('\n')
            utime.sleep(1)
        
        except KeyboardInterrupt:
            break
            
    print('Program Terminating')