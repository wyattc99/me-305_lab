# -*- coding: utf-8 -*-
"""
Created on Wed Nov 10 11:27:20 2021

@author: wyatt
"""

#Professor Demo to class code
#i2c = I2C(1, I2C.MASTER)
#i2c.mem_read(4, 0x28, 0x00)              #integer
#.mem_read(buf, 0x28, 0x00)              #buffer
#bytes_in = i2c.mem_read(4, 0x28, 0x00)
#bytes_in[0]
#hex(bytes_in[0])
#hex(bytes_in[1])
#hex(bytes_in[2])
#hex(bytes_in[3])
#buf = bytearray(4)
#buf
#bytes_in = i2c.mem_read(buf, 0x28, 0x00)
#buf
#buf = bytearray(6)
#i2c.mem_read(buf, 0x28, 0x1A)
#3D mode choose correct for euler theres 14 different thangs

#Bus 1 and Master
#i2c = I2C(1, I2C.MASTER)

#Bus 1 and Slave
#i2c = I2C(1, I2C.SLAVE)

#Method to change the operating mode
#whatever_were_writing = bytearray(1)
#i2c.mem_write(whatever_were_writing, 0x28, 0x3D)
              #register address operating mode = [OPR_MODE]
              #value operating mode = xxxx1100b
              #address operating mode = 0x3D
              #default value operating mode = 0x1C

#Calibration Status read
#buf_cal_status = bytearray(1)
#i2c.mem_read(buf_cal_status, 0x28, 0x1A)


#Calibration Coefficient read
#buf_cal_coef = bytearray(1)
#i2c.mem_read(buf_cal_coef, 0x28, 0x1A)


#Calibration Coefficient write
#i2c.mem_write(buf_cal_coef, 0x28, 0x1A)
#buf_cal_coef = bytearray(1)

#buf_euler = bytearray(6)       #the buf of euler data
#i2c.mem_read(buf_euler, 0x28, 0x1A)
      
            
#buf_ang = bytearray(6)   #the buf of angular velocity data

#i2c.mem_read(buf_ang, 0x28, 0x1A)

#Calibrating bytes section            
#cal_bytes = bytearray([0x5C])

#print("Binary:", '{:#010b}'.format(cal_bytes[0]))

#cal_status = (cal_bytes[0] & 0b11,
#            (cal_bytes[0] & 0b11 << 2) >> 2,
#             (cal_bytes[0] & 0b11 << 4) >> 4,
#             (cal_bytes[0] & 0b11 << 6) >> 6)

#print("Values:", cal_status)
#print('\n')

#Euler bytes section
#data1 = (0x00, 0x00, 0x00, 0x00, 0x00, 0x00)
#eul_bytes = bytearray([data1])

#Euler Data (Register Address, Default Value)
#EUL_Heading_LSB = 0x1A, 0x00
#EUL_Heading_MSB = 0x1B, 0x00
#EUL_Roll_LSB = 0x1C, 0x00
#EUL_Roll_MSB = 0x1D, 0x00
#EUL_Pitch_LSB = 0x1E, 0x00
#EUL_Pitch_MSB = 0x1F, 0x00

#print('Euler Bytes to Values')
#print('Raw bytes:', eul_bytes)

#eul_signed_ints = struct.unpack('<hhh', eul_bytes)
#print('Unpacked: ', eul_signed_ints)

#eul_vals = tuple(eul_int/16 for eul_int in eul_signed_ints)
#print('Scaled: ', eul_vals)

#Angular bytes section
#data2 = (0x00, 0x00, 0x00, 0x00, 0x00, 0x00)
#ang_bytes = bytearray(data2)

#Angular Velocity Data (Register Address, Default Value)
# GYR_DATA_Z_LSB = 0x18, 0x00
# GYR_DATA_Z_MSB = 0x19, 0x00
# GYR_DATA_Y_LSB = 0x16, 0x00
# GYR_DATA_Y_MSB = 0x17, 0x00
# GYR_DATA_X_LSB = 0x14, 0x00
# GYR_DATA_X_MSB = 0x15, 0x00

#print('Angular Velocity Bytes to Values')
#print('Raw bytes:', ang_bytes)

#ang_signed_ints = struct.unpack('<hhh', ang_bytes)
#print('Unpacked: ', ang_signed_ints)

#ang_vals = tuple(ang_int/16 for ang_int in ang_signed_ints)
#print('Scaled: ', ang_vals)

#Not sure if we need some of this stuff yet

# print whether the system is configured or not
        if self.cal_status[1] < 3:
            print('I2C System is NOT fully configured')
        elif self.cal_status[2] >= 3:
             print('I2C System is fully configured')
        
        # print whether the Gyroscope system is configured or not
        if self.cal_status[2] < 3:
            print('I2C Gyroscope is NOT configured')
        elif self.cal_status[2] >= 3:
             print('I2C Gyroscope is configured')    
        
        # print whether the Accelerometer system is configured or not
        if self.cal_status[3] < 3:
            print('I2C Accelerometer is NOT configured')
        elif self.cal_status[3] >= 3:
             print('I2C Accelermeter is configured')    
        
        # print whether the Magnetometer system is configured or not
        if self.cal_status[4] < 3:
            print('I2C Magnetometer is NOT configured')
        elif self.cal_status[4] >= 3:
             print('I2C Magnetometer is configured') 