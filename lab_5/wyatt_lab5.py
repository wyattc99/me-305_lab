# -*- coding: utf-8 -*-
"""
Created on Thu Nov  4 15:30:01 2021

@file       lavelle.py
@brief      This file will interact with a BNO055 Sensor
@details    This file interacts with a BNO055 Sensor in order to calibrate the
            sensor and from there either change the operating mode, read the
            euler angle data, or read the angular velocity data.
@author     James Lavelle Wyatt Conner
@date       11 November 2021
"""

import struct

class driver:
    
    def __init__(self, address, i2c):
        """
        @brief      This function will create an i2c object
        @details    This function will establish an i2c object that will work
                    throughout the class.
        @param i2c  The i2c object that will be associated with this class.
        """
<<<<<<< HEAD:lab_5/classdriver.py
        
        self.address = address
        self.i2c = i2c

    def set_operating_mode(self, mode):
=======
        ## This variable is to set the address of the I2C system
        self.I2C_addy = 0x28
        ## This variable is used to define number of byte arrays to read and write to
        self.buf = 0
        ## This variable is sued to define what mode of the operation
        self.MODE = 0
        #Bus 1 and Master
        self.i2c = I2C(1, I2C.MASTER)

          
    def change_operator(self, operation):
        """
        @brief      Allow the user to change the operation of IMU
        @details    This will allow the user to change the operation of the
                    IMU which will change what data the IMU collects. 
        @param      operation       This is the input variable used to determine what
                    operation mode the I2C will operate in the following are the different
                    opertion modes and their correlating values.
                     0  - Configmode
                     1  - Acconly
                     2  - magonly
                     3  - gyroonly
                     4  - accmag
                     5  - accgyro
                     6  - maggyr0
                     7  - amg
                     8  - IMU
                     9  - Compass
                     10 - M4G
                     11 - NDOF_FMC_OFF
                     12 - NDOF
          
                    
        """
      
        ## This variable is used to define the address to define our operation mode
        self.OPR_addy = 0x3D
    
        # writing to the I2C to set the operation mode
        self.i2c.mem_write(operation, self.I2C_addy, self.OPR_addy)
        
  #  def set_operating_mode(self, mode):
      #  """
      #  @br
       # @details
       # @param
      #  """
        
      #  self.i2c.mem_write(mode, 0x28, 0x3D)
        
    def set_units(self, units):
>>>>>>> bfe95f17b321be5cc9e181687942656492d6a060:lab_5/wyatt_lab5.py
        """
        @brief      This function will set the mode
        @details    This function will set the mode to NDOF at the number 12.
        @param mode The mode that the driver will need to be set at.
        """
        
        self.i2c.mem_write(mode, 0x28, 0x3D)
        
    def get_cal_status(self):
        """
        @brief      This will read data of calibration status of I2C unit
        @details    Depending upon the data read from the calibration status
                    we can determine if the system has been properly calibrated.
        """
<<<<<<< HEAD:lab_5/classdriver.py
=======
        #Calibration Status read
        ## Calibration Status to read from Adress
        self.cal_status_addy = 0x35
        self.buf = bytearray(1)
        self.i2c.mem_read(self.buf, self.I2C_addy , self.cal_status_addy)
>>>>>>> bfe95f17b321be5cc9e181687942656492d6a060:lab_5/wyatt_lab5.py
        
        buf_cal = bytearray(1)      
        cal_bytes = self.i2c.mem_read(buf_cal, 0x28, 0x35) 
        cal_status = (cal_bytes[0] & 0b11,
              (cal_bytes[0] & 0b11 << 2) >> 2,
              (cal_bytes[0] & 0b11 << 4) >> 4,
              (cal_bytes[0] & 0b11 << 6) >> 6)
        
<<<<<<< HEAD:lab_5/classdriver.py
        return cal_status   
=======
        print("Binary:", '{:#010b}'.format(self.buf[0]))

        cal_status = (self.buf[0] & 0b11,
             (self.buf[0] & 0b11 << 2) >> 2,
             (self.buf[0] & 0b11 << 4) >> 4,
             (self.buf[0] & 0b11 << 6) >> 6)
        
        if(cal_status[0] == 3 and cal_status[1] == 3 and cal_status[2] == 3 and cal_status[3] == 3):
            calibration_complete = True
           
            
        #elif(cal_status[0] == 0 and cal_status[1] == 0 and cal_status[2] == 0 and cal_status[3] == 0):
        #    calibration_complete = True

        else:
            calibration_complete = False
            print("Calibration Values:", cal_status)
            print('\n')
        return calibration_complete
           
>>>>>>> bfe95f17b321be5cc9e181687942656492d6a060:lab_5/wyatt_lab5.py
        
    def get_cal_coef(self):
        """
        @brief      This will return our calibration coefficents to allow us to properly read data
        @details    This allows for us read the calibration offsets for each sensor and will return
                    the data in an array format. 
        @return     This will return an array of the calibration coefficents. 
        """
<<<<<<< HEAD:lab_5/classdriver.py
        
        buf = bytearray(22)
        self.i2c.mem_read(buf, 0x28, 0x55)
        return tuple(struct.unpack('<hhhhhhhhhhh',buf))
=======
        self.offset_addy = 0x55
        self.buf = bytearray(18)
        
        self.offset_bytes = self.i2c.mem_read(self.buf, self.I2C_addy , self.offset_addy, timeout=5000)
        
        self.offset = struct.unpack('<hhhhhhhhh', self.offset_bytes)
        
        self.idx = 0
        print('Calibration Coefficents', self.offset)
     
        return self.offset
>>>>>>> bfe95f17b321be5cc9e181687942656492d6a060:lab_5/wyatt_lab5.py
        
    def write_cal_coef(self, cal_coef):
        """
        @brief      This will write to the calibration offsets
        @details    This will allow us to write to the calibration coefficents
                    to set them to what we want, as we can save the last calibration
                    status and use them again once we reset the system.
        @param      cal_coef  this is 18 byte input that will set the values 
                    of the calibration coefficents
                    
        """
        
        self.i2c.mem_write(cal_coef, 0x28, 0x55)
        pass
        
    def read_euler(self):
        """
        @brief      This will read the euler angle based off the I2C unit
        @details    This will read the euler angle data from the I2C unit.
                    It will read the data for all 3 axis of rotation, pitch
                    yawn and roll
        """
<<<<<<< HEAD:lab_5/classdriver.py
        
        self.buf_euler = bytearray(6)
        self.i2c.mem_read(self.buf_euler, 0x28, 0x1a)
        self.euler_ang = struct.unpack('hhh', self.buf_euler)
        return self.euler_ang

=======
        self.Euler_addy = 0x1A
        self.buf = bytearray(6)
        
        self.Euler_bytes = I2C.mem_read(self.buf, self.I2C_addy , self.Euler_addy, timeout=5000)
        
        self.Euler = struct.unpack('<hhh', self.Euler_bytes)
        
        return self.Euler
        
        
>>>>>>> bfe95f17b321be5cc9e181687942656492d6a060:lab_5/wyatt_lab5.py
    def read_angular(self):
        """
        @brief      This will read angualr veolicty data from the I2C unit
        @details    This will read the angular velocity data from the I2C unit
                    for all 3 axis of rotation, the output is a 3 number array.
                    
        """
        
<<<<<<< HEAD:lab_5/classdriver.py
        self.buf_ang = bytearray(6)
        self.i2c.mem_read(self.buf_ang, 0x28, 0x14)
        self.angular_vel = struct.unpack('hhh', self.buf_ang)
        return self.angular_vel
=======
        self.gyro_bytes = I2C.mem_read(self.buf, self.I2C_addy , self.gyro_addy, timeout=5000)
        
        self.gyro = struct.unpack('<hhh', self.gyro_bytes)
        
            
        return self.gyro
    
if __name__ == '__main__':
    '''
    @brief      Defining this file as the main file
    @details    setting up this file to be the main file of encoder file set. 
    '''
    runs = 0
    driver().change_operator(12)
    driver().set_units(0)

    
    while True:
        
        calibration = driver().retr_cal_status()
        if calibration == True:
            if runs == 0:
                driver().change_operator(0)
                Cal_coef = driver().retr_cal_coefficients()
                driver().change_operator(12)
                runs += 1
                print('Calibration Complete')
                print('\n')
                
       
            
            
        utime.sleep(1)    
    
    
>>>>>>> bfe95f17b321be5cc9e181687942656492d6a060:lab_5/wyatt_lab5.py
